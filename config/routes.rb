Rails.application.routes.draw do
  root 'group_events#index'

  resources :drafts, except: :index do
    member do
      post :publish
    end
  end

  resources :published_events, only: [ :edit, :update, :destroy ]
  resources :group_events, only: :index

  namespace :api do
    namespace :v1 do
      resources :drafts, only: [ :create, :update, :destroy ] do
        member do
          post :publish
        end
      end

      resources :published_events, only: [ :update, :destroy ]
      resources :group_events, only: :index
    end
  end
end
