ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)

abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'database_cleaner'
require 'capybara/rails'
require 'faker'
require 'factory_girl'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :firefox).tap do |driver|
    driver.browser.manage.window.resize_to('1980', '1024')
  end
end

Capybara.javascript_driver = :selenium
Capybara.server_port = 9887

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.use_transactional_fixtures = false

  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!

  config.include FactoryGirl::Syntax::Methods
  config.include Shoulda::Matchers::ActiveModel

  config.before :all do
    if self.class.metadata[:type] == :feature
      DatabaseCleaner.strategy = :truncation
      DatabaseCleaner.clean
    else
      DatabaseCleaner.strategy = :transaction
    end
  end

  config.before :suite do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before :each do |example|
     DatabaseCleaner.start
  end

  config.after :each do |example|
    DatabaseCleaner.clean
  end
end
