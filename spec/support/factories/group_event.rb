FactoryGirl.define do
  factory :draft, class: GroupEvent do
    title { Faker::RockBand.name }
    location { Faker::Address.street_address }
    description { Faker::Lorem.paragraph }
    starts_at { 10.days.from_now }
    duration { GroupEvent::DURATIONS.first }
  end

  factory :published_event, class: GroupEvent do
    title { Faker::RockBand.name }
    location { Faker::Address.street_address }
    description { Faker::Lorem.paragraph }
    starts_at { 10.days.from_now }
    duration { GroupEvent::DURATIONS.first }
    event_type { GroupEvent.event_type.published }
  end
end
