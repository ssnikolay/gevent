shared_examples 'group event response' do
  let(:json_body) { JSON.parse(response.body) }

  it 'should return serialize draft' do
    expect(response).to be_success
    expect(json_body).to eq({
      'id' => group_event.id,
      'event_type' => group_event.event_type,
      'title' => group_event.title,
      'duration' => group_event.duration,
      'location' => group_event.location,
      'description' => group_event.description,
      'starts_at' => group_event.starts_at.as_json
    })
  end
end
