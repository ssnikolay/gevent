describe Drafts::Form do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_inclusion_of(:duration).in_array(GroupEvent::DURATIONS).allow_blank(true) }
end
