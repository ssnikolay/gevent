describe PublishedEvents::Form do
  it { is_expected.to validate_inclusion_of(:duration).in_array(GroupEvent::DURATIONS) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:location) }
  it { is_expected.to validate_presence_of(:starts_at) }
  it { is_expected.to validate_presence_of(:duration) }
end
