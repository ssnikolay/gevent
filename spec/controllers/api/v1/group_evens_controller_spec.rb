describe Api::V1::GroupEventsController do
  let(:json_body) { JSON.parse(response.body) }

  describe 'GET .index' do
    let!(:published_event) { create(:published_event) }
    let!(:draft) { create(:draft) }

    before { get :index }

    subject { JSON.parse(response.body)['group_events'] }

    its(:count) { is_expected.to eq 2 }
  end
end
