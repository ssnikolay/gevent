describe Api::V1::PublishedEventsController do
  shared_examples 'dont process draft' do
    let(:published_event) { create(:draft) }
    let(:params) { {} }

    before { execute_request }

    subject { response }

    it { is_expected.to be_not_found }
  end

  let(:json_body) { JSON.parse(response.body) }

  describe 'PUT #update' do
    let(:published_event) { create(:published_event) }
    let(:execute_request) { put :update, params: { id: published_event.id, published_event: params } }

    it_behaves_like 'dont process draft'

    context 'when params is valid' do
      let(:params) { { title: 'New title' } }

      it 'should update published_event attributes' do
        expect { execute_request }.to change { published_event.reload.title }.to(params[:title])
      end

      describe 'response' do
        let(:group_event) { published_event.reload }

        before { execute_request }

        it_behaves_like 'group event response'
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:published_event) { create(:published_event) }
    let(:execute_request) { delete :destroy, params: { id: published_event.id } }

    it_behaves_like 'dont process draft'

    it 'should mark published_event as deleted' do
      expect { execute_request }.to change { published_event.reload.deleted }.to(true)
    end

    describe 'response' do
      let(:group_event) { published_event.reload }

      before { execute_request }

      subject { response }

      its(:status) { is_expected.to eq(204) }
    end
  end
end
