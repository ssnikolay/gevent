describe Api::V1::DraftsController do
  shared_examples 'dont process published events' do
    let(:draft) { create(:published_event) }
    let(:params) { {} }

    before { execute_request }

    subject { response }

    it { is_expected.to be_not_found }
  end

  let(:json_body) { JSON.parse(response.body) }

  describe 'POST .create' do
    let(:execute_request) { post :create, params: { draft: params } }

    context 'when params is valid' do
      let(:params) do
        attributes = build(:draft).attributes
        attributes.slice(*Drafts::Form.new.form_attributes)
      end

      describe 'response' do
        let(:group_event) { GroupEvent.last }

        before { execute_request }

        it_behaves_like 'group event response'
      end

      describe 'attributes' do
        before { execute_request }

        subject { GroupEvent.last }

        it 'should create GroupEvent' do
          params.each do |attribute, value|
            expect(subject.public_send(attribute)).to eq(value)
          end
          expect(subject).to be_draft
        end
      end
    end

    context 'when params is invalid' do
      let(:params) { { title: nil }}

      before { execute_request }

      it 'should return errors' do
        expect(response.status).to eq(422)
        expect(json_body).to eq({'errors' => { 'title' => ["can't be blank"]}})
      end
    end
  end

  describe 'PUT #update' do
    let(:draft) { create(:draft) }
    let(:execute_request) { put :update, params: { id: draft.id, draft: params } }

    it_behaves_like 'dont process published events'

    context 'when params is valid' do
      let(:params) { { title: 'New title' } }

      it 'should update draft attributes' do
        expect { execute_request }.to change { draft.reload.title }.to(params[:title])
      end

      describe 'response' do
        let(:group_event) { draft.reload }

        before { execute_request }

        it_behaves_like 'group event response'
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:draft) { create(:draft) }
    let(:execute_request) { delete :destroy, params: { id: draft.id } }

    it_behaves_like 'dont process published events'

    it 'should mark draft as deleted' do
      expect { execute_request }.to change { draft.reload.deleted }.to(true)
    end

    describe 'response' do
      let(:group_event) { draft.reload }

      before { execute_request }

      subject { response }

      its(:status) { is_expected.to eq(204) }
    end
  end

  describe 'POST #publish' do
    let(:draft) { create(:draft) }
    let(:execute_request) { post :publish, params: { id: draft.id } }

    context 'when draft already published' do
      let(:draft) { create(:published_event) }

      before { execute_request }

      it 'should return errors' do
        expect(response.status).to eq(422)
        expect(json_body).to eq({'errors' => { 'base' => ['This Group Event already published.']}})
      end
    end

    context 'when draft not published' do
      before do
        expect_any_instance_of(PublishedEvents::Form).to receive(:valid?).and_return(valid)
      end

      context 'when draft is valid' do
        let(:valid) { true }

        it 'should publish draft' do
          expect { execute_request }.to change { draft.reload.event_type }.to(GroupEvent.event_type.published)
        end

        describe 'response' do
          let(:group_event) { draft.reload }

          before { execute_request }

          it_behaves_like 'group event response'
        end
      end

      context 'when draft is invalid' do
        let(:valid) { false }

        before { execute_request }

        it 'should return errors' do
          expect(response.status).to eq(422)
          expect(json_body).to eq({'errors' => { 'base' => ['Group Event publish failed. Edit draft and try again.']}})
        end
      end
    end
  end
end
