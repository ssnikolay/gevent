describe PublishedEventsController do
  shared_examples 'dont process draft' do
    let(:published_event) { create(:draft) }
    let(:params) { {} }

    it 'should return record not found' do
      expect { execute_request }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe 'PUT #update' do
    let(:published_event) { create(:published_event) }
    let(:execute_request) { put :update, params: { id: published_event.id, published_event: params } }

    it_behaves_like 'dont process draft'

    context 'when params is valid' do
      let(:params) { { title: 'New title' } }

      it 'should update published_event attributes' do
        expect { execute_request }.to change { published_event.reload.title }.to(params[:title])
      end
    end

    context 'when params is invalid' do
      let(:params) { { title: nil }}

      describe 'response' do
        subject { execute_request }

        its(:status) { is_expected.to eq(200) }
      end

      it 'should not update GroupEvent' do
        expect { execute_request }.not_to change{ published_event.reload.title }
      end
    end
  end

  describe 'DELETE #destoy' do
    let(:published_event) { create(:published_event) }
    let(:execute_request) { delete :destroy, params: { id: published_event.id } }

    it_behaves_like 'dont process draft'

    it 'should mark published event as deleted' do
      expect { execute_request }.to change { published_event.reload.deleted }.to(true)
    end
  end
end
