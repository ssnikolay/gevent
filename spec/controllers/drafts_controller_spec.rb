describe DraftsController do
  shared_examples 'dont process published events' do
    let(:draft) { create(:published_event) }
    let(:params) { {} }

    it 'should return record not found' do
      expect { execute_request }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe 'POST .create' do
    let(:execute_request) { post :create, params: { draft: params } }

    context 'when params is valid' do
      let(:params) do
        attributes = build(:draft).attributes
        attributes.slice(*Drafts::Form.new.form_attributes)
      end

      describe 'response' do
        subject { execute_request }

        its(:status) { is_expected.to eq(302) }
      end

      describe 'attributes' do
        before { execute_request }

        subject { GroupEvent.last }

        it 'should create GroupEvent' do
          params.each do |attribute, value|
            expect(subject.public_send(attribute)).to eq(value)
          end
          expect(subject).to be_draft
        end
      end
    end

    context 'when params is invalid' do
      let(:params) { { title: nil }}

      describe 'response' do
        subject { execute_request }

        its(:status) { is_expected.to eq(200) }
      end

      describe 'GroupEvent persistence' do
        it 'should not create GroupEvent' do
          expect { execute_request }.not_to change{ GroupEvent.count }
        end
      end
    end
  end

  describe 'PUT #update' do
    let(:draft) { create(:draft) }
    let(:execute_request) { put :update, params: { id: draft.id, draft: params } }

    it_behaves_like 'dont process published events'

    context 'when params is valid' do
      let(:params) { { title: 'New title' } }

      it 'should update draft attributes' do
        expect { execute_request }.to change { draft.reload.title }.to(params[:title])
      end
    end
  end

  describe 'DELETE #destoy' do
    let(:draft) { create(:draft) }
    let(:execute_request) { delete :destroy, params: { id: draft.id } }

    it_behaves_like 'dont process published events'

    it 'should mark draft as deleted' do
      expect { execute_request }.to change { draft.reload.deleted }.to(true)
    end
  end

  describe 'POST #publish' do
    let(:draft) { create(:draft) }
    let(:execute_request) { post :publish, params: { id: draft.id } }

    context 'when draft already published' do
      let(:draft) { create(:published_event) }

      before { execute_request }

      it 'should return fail message' do
        expect(flash[:error]).to eq('This Group Event already published.')
      end
    end

    context 'when draft not published' do
      before do
        expect_any_instance_of(PublishedEvents::Form).to receive(:valid?).and_return(valid)
      end

      context 'when draft is valid' do
        let(:valid) { true }

        it 'should publish draft' do
          expect { execute_request }.to change { draft.reload.event_type }.to(GroupEvent.event_type.published)
          expect(flash[:notice]).to eq('Group Event published successfully.')
        end
      end

      context 'when draft is invalid' do
        let(:valid) { false }

        it 'should not publish draft' do
          expect { execute_request }.not_to change { draft.reload.event_type }
          expect(flash[:error]).to eq('Group Event publish failed. Edit draft and try again.')
        end
      end
    end
  end
end
