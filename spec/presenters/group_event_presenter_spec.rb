describe GroupEventPresenter do
  subject { described_class.new(event) }

  describe 'when event is draft' do
    let(:event) { build(:draft, description: "We want to put a paragraph...\n\n...right there.") }

    its(:title) { is_expected.to eq event.title }
    its(:location) { is_expected.to eq event.location }
    its(:subject) { is_expected.to eq event }
    its(:label) { is_expected.to eq 'Draft' }
    its(:description) { is_expected.to eq "<p>We want to put a paragraph...</p>\n\n<p>...right there.</p>" }

  end

  describe 'when event is published event' do
    let(:event) { build(:published_event) }

    its(:label) { is_expected.to eq 'Published' }
  end

  describe 'ends_at' do
    context 'when duration is nil' do
      let(:event) { build(:draft, duration: nil) }

      its(:ends_at) { is_expected.to be_nil }
    end

    context 'when starts at is nil' do
      let(:event) { build(:draft, starts_at: nil) }

      its(:ends_at) { is_expected.to be_nil }
    end

    context 'when srarts_at and duration exists' do
      let(:event) { build(:published_event) }

      its(:ends_at) { is_expected.to eq((event.starts_at + 30).as_json) }
    end
  end
end
