describe 'Creation Draft features', js: true do
  let!(:draft) { build(:draft) }

  scenario 'As User I can create new Draft' do
    visit group_events_path

    click_link 'Add'
    within('#new_draft') do
      fill_in 'Title', with: draft.title
      fill_in 'Location', with: draft.location
      fill_in 'Description', with: draft.description
      fill_in 'Start date', with: draft.starts_at
      select(draft.duration, from: 'Duration')
    end
    click_button 'Save'

    expect(page).to have_content 'Group Event created successfully'
  end
end
