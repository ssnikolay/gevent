describe 'Update Draft features', js: true do
  let!(:draft) { create(:draft) }
  let(:new_title) { 'New title' }

  scenario 'As User I can update existing Draft' do
    visit group_events_path

    click_link draft.title
    within('#new_draft') do
      fill_in 'Title', with: new_title
    end
    click_button 'Save'

    expect(page).to have_content 'Group Event updated successfully'
  end
end
