describe 'Update Published event features', js: true do
  let!(:published_event) { create(:published_event) }
  let(:new_title) { 'New title' }

  scenario 'As User I can update existing Published event' do
    visit group_events_path

    click_link published_event.title
    within('#new_published_event') do
      fill_in 'Title', with: new_title
    end
    click_button 'Save'

    expect(page).to have_content 'Group Event updated successfully'
  end
end
