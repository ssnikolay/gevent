# Gevent

https://gevents.herokuapp.com/

## Installation:

1. Install ruby-2.3.0
1. `$ git clone git@bitbucket.org:ssnikolay/gevent.git`
1. copy ./config/database.yml.sample to ./config/datatbase.yml
1. `$ rake db:create db:migrate`

## Run:

1. `$ rails s`
1. Open `localhost:3000` in your browser

## Tests:

1. `$ RAILS_ENV=test rake db:create db:migrate`
1. Run all test: `$ rspec`


## JSON API:

GroupEvents API:

1. Get all group events: `curl http://localhost:3000/api/v1/group_events`

Drafts API:

1. Create: `curl -X POST http://localhost:3000/api/v1/drafts --data "draft[title]='New Draft'"`
1. Update: `curl -X PUT http://localhost:3000/api/v1/drafts/:id --data "draft[title]='Different title'"`
1. Delete: `curl -X DELETE http://localhost:3000/api/v1/drafts/:id"`
1. Publish: `curl -X POST http://localhost:3000/api/v1/drafts/:id/publish"`

Published Events API:

1. Update: `curl -X PUT http://localhost:3000/api/v1/published_events/:id --data "draft[title]='Different title'"`
1. Delete: `curl -X DELETE http://localhost:3000/api/v1/published_events/:id"`

## FYI:

I have a small remark about the task.
`Write an AR model, spec and migration for a GroupEvent` limited me in a decision about database architecture. If I would get this task in "real life",
I would discuss with a team (and maybe with PM) about possibility of separation domain model GroupEvent
on the two DB tables: Draft, PublishedEvent.

It would give us these pros:

1. Not to track states of GroupEvents.
2. Purer controller layer.
3. Restrictions on database layer (not null constraints of all PublishedEvent attributes).
4. More flexible architecture for new features.
For example, coexistence draft and published event and their management like Mandrill Templates.

So, I didn't add two tables, but I split controller layer on the two resources (domain entities): PublishedEvent and Draft.

With regard to API, in real production, you need to know who will use API.
If it is a mobile application where UI for Drafts and PublishedEvent is different, my solution would be more convenient.
So, I repeated the "html controllers", and this, in part, is for speed up implementation :)
