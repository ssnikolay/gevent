module Api
  class ApplicationController < ActionController::API
    rescue_from ActiveRecord::RecordNotFound, with: :not_found

    def not_found
      respond_with_errors(['Not found'], :not_found)
    end

    def respond_with_errors(errors, status = :unprocessable_entity)
      render json: { errors: errors }, status: status
    end
  end
end
