module Api
  module V1
    class DraftsController < Api::ApplicationController
      def create
        form = Drafts::Form.new
        form.attributes = permitted_params(form)

        if form.valid? && draft = repository.create(form.attributes)
          render_draft(draft)
        else
          respond_with_errors(form.errors)
        end
      end

      def update
        draft = repository.find(params[:id])
        form = Drafts::Form.new(draft.attributes)
        form.attributes = permitted_params(form)

        if form.valid? && draft.update_attributes(form.attributes)
          render_draft(draft)
        else
          respond_with_errors(form.errors)
        end
      end

      def destroy
        draft = repository.find(params[:id])
        if draft.mark_as_deleted
          head :no_content
        else
          respond_with_errors([ I18n.t('group_event.failed_delete') ])
        end
      end

      def publish
        draft = GroupEvent.find(params[:id])
        if draft.published?
          respond_with_errors(base: [ I18n.t('group_event.already_published') ])
        else
          form = PublishedEvents::Form.new(draft.attributes)
          if form.valid? && draft.publish
            render_draft(draft)
          else
            respond_with_errors(base: [ I18n.t('group_event.failed_publish') ])
          end
        end
      end

      private

      def repository
        GroupEvent.draft
      end

      def render_draft(draft)
        render json: GroupEventSerializer.new(draft)
      end

      def permitted_params(form)
        params.require(:draft).permit(form.form_attributes).to_h
      end
    end
  end
end
