module Api
  module V1
    class PublishedEventsController < Api::ApplicationController
      def update
        published_event = repository.find(params[:id])
        form = PublishedEvents::Form.new(published_event.attributes)
        form.attributes = permitted_params(form)

        if form.valid? && published_event.update_attributes(form.attributes)
          render_published_event(published_event)
        else
          respond_with_errors(base: [ I18n.t('group_event.failed_update') ])
        end
      end

      def destroy
        published_event = repository.find(params[:id])
        if published_event.mark_as_deleted
          head :no_content
        else
          respond_with_errors(base: [ I18n.t('group_event.failed_delete') ])
        end
      end

      private

      def repository
        GroupEvent.published
      end

      def render_published_event(published_event)
        render json: GroupEventSerializer.new(published_event)
      end

      def permitted_params(form)
        params.require(:published_event).permit(form.form_attributes).to_h
      end
    end
  end
end
