module Api
  module V1
    class GroupEventsController < ApplicationController
      def index
        render json: { group_events: GroupEventSerializer.map(GroupEvent.all) }
      end
    end
  end
end
