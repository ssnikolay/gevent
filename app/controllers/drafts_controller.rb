class DraftsController < ApplicationController
  def new
    @form = Drafts::Form.new
  end

  def create
    @form = Drafts::Form.new
    @form.attributes = permitted_params(@form)

    if @form.valid? && repository.create(@form.attributes)
      redirect_to root_path, notice: t('group_event.successfully_created')
    else
      flash.now[:error] = t('group_event.failed_creation')
      render :new
    end
  end

  def edit
    @draft = repository.find(params[:id])
    @form = Drafts::Form.new(@draft.attributes)
  end

  def update
    @draft = repository.find(params[:id])
    @form = Drafts::Form.new(@draft.attributes)
    @form.attributes = permitted_params(@form)

    if @form.valid? && @draft.update_attributes(@form.attributes)
      redirect_to root_path, notice: t('group_event.successfully_updated')
    else
      flash.now[:error] = t('group_event.failed_update')
      render :edit
    end
  end

  def destroy
    @draft = repository.find(params[:id])
    if @draft.mark_as_deleted
      redirect_to root_path, notice: t('group_event.successfully_deleted')
    else
      redirect_to root_path, flash: { error: t('group_event.failed_delete') }
    end
  end

  def publish
    @draft = GroupEvent.find(params[:id])
    if @draft.published?
      redirect_to root_path, flash: { error: t('group_event.already_published') }
    else
      @form = PublishedEvents::Form.new(@draft.attributes)
      if @form.valid? && @draft.publish
        redirect_to root_path, notice: t('group_event.successfully_published')
      else
        flash.now[:error] = t('group_event.failed_publish')
        render :edit
      end
    end
  end

  private

  def repository
    GroupEvent.draft
  end

  def permitted_params(form)
    params.require(:draft).permit(form.form_attributes).to_h
  end
end
