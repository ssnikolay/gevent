class PublishedEventsController < ApplicationController
  def edit
    @published_event = repository.find(params[:id])
    @form = PublishedEvents::Form.new(@published_event.attributes)
  end

  def update
    @published_event = repository.find(params[:id])
    @form = PublishedEvents::Form.new(@published_event.attributes)
    @form.attributes = permitted_params(@form)

    if @form.valid? && @published_event.update_attributes(@form.attributes)
      redirect_to root_path, notice: t('group_event.successfully_updated')
    else
      flash.now[:error] = t('group_event.failed_update')
      render :edit
    end
  end

  def destroy
    @published_event = repository.find(params[:id])
    if @published_event.mark_as_deleted
      redirect_to root_path, notice: t('group_event.successfully_deleted')
    else
      redirect_to root_path, flash: { error: t('group_event.failed_delete') }
    end
  end

  private

  def repository
    GroupEvent.published
  end

  def permitted_params(form)
    params.require(:published_event).permit(form.form_attributes).to_h
  end
end
