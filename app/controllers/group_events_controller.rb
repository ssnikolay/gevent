class GroupEventsController < ApplicationController
  def index
    @published_events = GroupEventPresenter.map(GroupEvent.published)
    @drafts = GroupEventPresenter.map(GroupEvent.draft)
  end
end
