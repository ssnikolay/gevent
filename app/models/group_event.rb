class GroupEvent < ApplicationRecord
  extend Enumerize
  DURATIONS = [30, 60]

  enumerize :event_type, in: %i(draft published),
    default: :draft,
    scope: true,
    predicates: true

  scope :not_deleted, -> { where(deleted: false) }
  scope :published, -> { not_deleted.with_event_type(:published) }
  scope :draft, -> { not_deleted.with_event_type(:draft) }

  def mark_as_deleted
    update_attributes(deleted: true)
  end

  def publish
    update_attributes(event_type: self.class.event_type.published)
  end
end
