module PublishedEvents
  class Form < Drafts::Form
    validates :description, :location, :starts_at, :duration, presence: true
    validates :duration, inclusion: { in: GroupEvent::DURATIONS }
  end
end
