class BaseForm
  include ActiveModel::Naming
  include ActiveModel::Model

  include Virtus.model

  def form_attributes
    attributes.stringify_keys.keys
  end
end
