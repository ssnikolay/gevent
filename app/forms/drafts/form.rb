module Drafts
  class Form < BaseForm
    attribute :title,       String
    attribute :description, String
    attribute :location,    String
    attribute :starts_at,   Date
    attribute :duration,    Integer

    validates :title, presence: true
    validates :duration, inclusion: { in: GroupEvent::DURATIONS, allow_blank: true }

    def duration_collection
      GroupEvent::DURATIONS
    end
  end
end
