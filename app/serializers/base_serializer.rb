class BaseSerializer < ActiveModel::Serializer
  class << self
    def map(arr, options = {})
      ActiveModel::Serializer::CollectionSerializer.new(arr, { serializer: self }.merge(options) )
    end
  end
end
