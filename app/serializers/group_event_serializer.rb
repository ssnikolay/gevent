class GroupEventSerializer < BaseSerializer
  attributes :id, :event_type, :title, :location, :starts_at, :duration, :description
end
