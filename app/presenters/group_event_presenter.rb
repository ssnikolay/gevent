class GroupEventPresenter < BasePresenter
  include ActionView::Helpers::TextHelper

  delegate :title, :location, to: :@subject
  attr_reader :subject

  def initialize(draft)
    @subject = draft
  end

  def label
    @subject.event_type_text
  end

  def description
    simple_format(@subject.description)
  end

  def ends_at
    (@subject.starts_at + @subject.duration.days).as_json if @subject.duration && @subject.starts_at
  end
end
