class BasePresenter
  class << self
    def map(arr)
      arr.map { |item| new(item) }
    end
  end
end
