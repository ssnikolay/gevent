class CreateGroupEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :group_events do |t|
      t.string :title
      t.text :description
      t.string :location
      t.date :starts_at
      t.integer :duration
      t.string :event_type, null: false
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
